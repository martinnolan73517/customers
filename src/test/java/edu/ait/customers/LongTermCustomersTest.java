package edu.ait.customers;

import edu.ait.customers.dao.CustomerDAO;
import edu.ait.customers.dto.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class LongTermCustomersTest {
    int longTermCustTest = CustomerDAO.longTermCust;
    int oneMoreThanLongTermCustTest = longTermCustTest+1;
    int oneLessThanLongTermCustTest = longTermCustTest-1;

    private Customer customer;

    @BeforeEach
    public void setUp(){
        customer = new Customer(0, "John Smith", "jsmith99@gmail.com", oneMoreThanLongTermCustTest);
    }

    @Test
    public void testTrue(){
        if(customer.getYearsCustomer()>=longTermCustTest) {
            assertEquals(true, customer.getLongTermCustomer());
        } else assertEquals(false, customer.getLongTermCustomer());
    }

    @Test
    public void testFalse(){
        customer.setYearsCustomer(oneLessThanLongTermCustTest);
        if(customer.getYearsCustomer()>=longTermCustTest) {
            assertEquals(true, customer.getLongTermCustomer());
        } else assertEquals(false, customer.getLongTermCustomer());
        //assertEquals(false , customer.getLongTermCustomer());
    }


}
