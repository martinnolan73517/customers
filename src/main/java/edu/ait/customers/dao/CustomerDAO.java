package edu.ait.customers.dao;

import edu.ait.customers.dto.Customer;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class CustomerDAO {
    public static int longTermCust = 5;

    private List<Customer> customerList = new ArrayList(Arrays.asList(
            new Customer(0, "John Jones 27", "jsmith99@gmail.com", 8),
            new Customer(1, "Davy Jones", "djones1234@gmail.com", 3),
            new Customer(2, "Mary O'Brien", "mob12345@gmail.com",6),
            new Customer(3, "Bill Gates", "bill987@gmail.com",1)
    ));

    private Customer findCustomer(int customerId){
        Customer found = null;
        for(Customer customer: customerList){
            if(customer.getId() == customerId){
                found = customer;
                break;
            }
        }
//        System.out.println(found);
//        System.err.println(found);
        return found;
    }


    public List<Customer> findAll(){
        return customerList;
    }

    public Optional<Customer> findById(int id){
        return Optional.ofNullable(findCustomer(id));
    }

    public List<Customer> findOver5Years(){
        List<Customer> over5years = new ArrayList(Arrays.asList());

        for (Customer customer: customerList){
            if(customer.getYearsCustomer()>=longTermCust){
                over5years.add(customer);
            }
        }
        return over5years;
    }

    public List<Customer> longerThanXYears(int years){
        List<Customer> over5years = new ArrayList(Arrays.asList());

        for (Customer customer: customerList){
            if(customer.getYearsCustomer()>=years){
                over5years.add(customer);
            }
        }
        return over5years;
    }

    public String createCustomer(Customer newCustomer){
        if(newCustomer.getYearsCustomer()>=longTermCust){
            newCustomer.setLongTermCustomer(true);
            customerList.add(newCustomer);
            return "Long term customer";
        } else{
            newCustomer.setLongTermCustomer(false);
            customerList.add(newCustomer);
            return "Short term customer";
        }



    }

//    @PostMapping("wines")
//    public ResponseEntity createNewWine(@RequestBody Wine newWine){
//        wineRepository.save(newWine);
//
//        URI location = ServletUriComponentsBuilder
//                .fromCurrentRequest().path("{id}")
//                .buildAndExpand(newWine.getId()).toUri();
//        return ResponseEntity.created(location).build();
//    }

}
