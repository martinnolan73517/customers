package edu.ait.customers.dto;

import edu.ait.customers.dao.CustomerDAO;

public class Customer {
    private int longTerm = CustomerDAO.longTermCust;

    private int id;
    private String name;
    private String email;
    private int yearsCustomer;
    private boolean longTermCustomer;

    public Customer() {
    }

    public Customer(int id, String name, String email, int yearsCustomer) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.yearsCustomer = yearsCustomer;
        if(yearsCustomer>=longTerm){
            this.longTermCustomer = true;
        } else{
            this.longTermCustomer = false;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getYearsCustomer() {
        return yearsCustomer;
    }

    public void setYearsCustomer(int yearsCustomer) {
        if(yearsCustomer>=5){
            setLongTermCustomer(true);
        } else setLongTermCustomer(false);
        this.yearsCustomer = yearsCustomer;
    }

    public boolean getLongTermCustomer() {
        return longTermCustomer;
    }

    public void setLongTermCustomer(boolean longTermCustomer) {
        this.longTermCustomer = longTermCustomer;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", yearsCustomer=" + yearsCustomer +
                ", longTermCustomer=" + longTermCustomer +
                '}';
    }
}
