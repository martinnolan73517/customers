package edu.ait.customers.controllers;

import edu.ait.customers.dao.CustomerDAO;
import edu.ait.customers.dto.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class CustomerController  {

    @Autowired
    CustomerDAO customerDAO;

    @GetMapping("customers")
    public List<Customer> getAllCustomers(){
        return customerDAO.findAll();
    }

    @GetMapping("customers/{id}")
    public Optional<Customer> getCustoemrById(@PathVariable int id){
        return customerDAO.findById(id);
    }

    @GetMapping("customers/longer-than/{years}")
    public List<Customer> getLongerThanCustomers(@PathVariable int years){
        //System.err.println("inside");
        return customerDAO.longerThanXYears(years);
        //return "hello";
    }

    @GetMapping("customers/long-term")
    public List<Customer> getLongTermCustomers(){
        return customerDAO.findOver5Years();
    }

    @PostMapping("customers")
    public ResponseEntity createNewCustomer(@RequestBody Customer newCustomer){
        customerDAO.createCustomer(newCustomer);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("{id}")
                .buildAndExpand(newCustomer.getId()).toUri();
        return ResponseEntity.created(location).build();
    }
//    @PostMapping("wines")
//    public ResponseEntity createNewWine(@RequestBody Wine newWine){
//        wineRepository.save(newWine);
//
//        URI location = ServletUriComponentsBuilder
//                .fromCurrentRequest().path("{id}")
//                .buildAndExpand(newWine.getId()).toUri();
//        return ResponseEntity.created(location).build();
//    }

}
